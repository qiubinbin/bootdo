package com.boot.common.service;

import org.springframework.stereotype.Service;

import com.boot.common.domain.LogDO;
import com.boot.common.domain.PageDO;
import com.boot.common.utils.Query;
@Service
public interface LogService {
	void save(LogDO logDO);
	PageDO<LogDO> queryList(Query query);
	int remove(Long id);
	int batchRemove(Long[] ids);
}
