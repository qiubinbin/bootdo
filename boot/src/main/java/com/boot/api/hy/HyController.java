package com.boot.api.hy;

import com.boot.api.utils.ApiResUtil;
import com.boot.common.utils.IdGenerate;
import com.boot.common.utils.PageUtils;
import com.boot.common.utils.Query;
import com.boot.common.utils.R;
import com.boot.system.domain.AnchorDO;
import com.boot.system.domain.CommentDO;
import com.boot.system.domain.HyUserDO;
import com.boot.system.domain.MissDO;
import com.boot.system.service.AnchorService;
import com.boot.system.service.CommentService;
import com.boot.system.service.HyUserService;
import com.boot.system.service.MissService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/hy")
@CrossOrigin
public class HyController {
    @Autowired
    MissService missService;
    @Autowired
    CommentService commentService;
    @Autowired
    HyUserService hyUserService;
    @Autowired
    AnchorService anchorService;
//    寻人列表
    @RequestMapping("missList")
    public ApiResUtil missList(@RequestParam Map<String, Object> params)
    {
        //查询列表数据
        Query query = new Query(params);
        List<MissDO> missList = missService.list(query);
        int total = missService.count(query);
        PageUtils pageUtils = new PageUtils(missList, total);
        return ApiResUtil.succeed(pageUtils);
    }
    //    添加寻人信息
    @RequestMapping("missAdd")
    public ApiResUtil missAdd(MissDO miss)
    {
        miss.setId(IdGenerate.uuid());
        miss.setCreateDate(new Date());
        if(missService.save(miss)>0){
            return ApiResUtil.succeed();
        }
        return ApiResUtil.fail();

    }
    //添加回复
    @RequestMapping("commentAdd")
    public ApiResUtil commentAdd(CommentDO commentDO)
    {
        commentDO.setId(IdGenerate.uuid());
        commentDO.setCreateDate(new Date());
        if(commentService.save(commentDO)>0){
            return ApiResUtil.succeed();
        }
        return ApiResUtil.fail();

    }
    //寻人详情
    @RequestMapping("findMissById")
    public ApiResUtil findMissById(@RequestParam("mid") String mid)
    {
        Map<String,Object> date=new HashMap<>();

//        MissDO missDO= missService.get(mid);
        List<CommentDO> commentDOList=commentService.findListByMid(mid);
//        ------------------------------------
        date.put("commentList",commentDOList);
        return ApiResUtil.succeed(date);

    }
    //添加用户
    @RequestMapping("addUser")
    public ApiResUtil addUser(HyUserDO hyUserDO)
    {
        hyUserDO.setId(IdGenerate.uuid());
        hyUserDO.setCreateDate(new Date());
        if(hyUserService.save(hyUserDO)>0){
            return ApiResUtil.succeed();
        }
        return ApiResUtil.succeed();

    }
    //当前用户数
    @RequestMapping("findUserCount")
    public ApiResUtil findUserCount(HyUserDO hyUserDO)
    {
        Query query = new Query(new HashMap<>());

        int total = hyUserService.count(query);
        return ApiResUtil.succeed(total);

    }
    //添加主播
    @RequestMapping("addAnchor")
    public ApiResUtil addAnchor(AnchorDO anchorDO)
    {
        anchorDO.setId(IdGenerate.uuid());
        anchorDO.setCreateDate(new Date());
        if(anchorService.save(anchorDO)>0){
            return ApiResUtil.succeed();
        }
        return ApiResUtil.succeed();

    }
    //当前参与主播数
    @RequestMapping("findAnchorCount")
    public ApiResUtil findAnchorCount()
    {
        Query query = new Query(new HashMap<>());

        int total = anchorService.count(query);
        return ApiResUtil.succeed(total);


    }
    //获取主播列表
    @RequestMapping("findAnchorList")
    public ApiResUtil findAnchorList(@RequestParam Map<String, Object> params)
    {
        return ApiResUtil.succeed();

    }

//    //    关键字搜索
//    @RequestMapping("missSearch")
//    public ApiResUtil missSearch(@RequestParam("key") String key)
//    {
//        miss.setId(IdGenerate.uuid());
//        if(missService.save(miss)>0){
//            return ApiResUtil.succeed();
//        }
//        return ApiResUtil.fail();
//
//    }

}
