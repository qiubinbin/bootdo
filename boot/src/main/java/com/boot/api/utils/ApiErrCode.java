package com.boot.api.utils;

/**
 * Created by weimingjin on 2018/10/9.
 */

public enum ApiErrCode {
    /**
     * 系统错误*/
    SUCCESS(0,"成功"),
    ERROR(500,"系统错误"),
    TOKEN_INVALID(401,"TOKEN_IS_INVALID"),
    REPEAT_PHONE_UPDATE(10001, "手机号不能重复"),
    REPEAT_NAME_UPDATE(10002, "名称不能重复"),
    NAME_IS_NULL(10003, "名称不能为空"),
    NAME_OR_PASS_ERROR(10004, "用户名或密码错误"),
    PARAM_OBJECT_ISNULL(10005,"参数对象为null"),
    PARAM_ISNULL(10005,"参数为空"),

/**
 * 商家错误*/
    MERCHANT_NULL(20001,"商户不存在"),

    USER_NULL(20003,"用户不存在"),
    MERCHANT_NO_ADOPT(20002,"商户未审核"),
    USERPAYING(20012,"正在付款中"),
    OREDER_EXECPTION(20003,"订单异常"),


    VERSION_SUCCESS(30001,"已经是最新版本"),
/**
 * 新增地点错误*/

    /**
     * 投票错误码 start
     */
    ACTIVITY_END(91001,"活动已结束"),
    MAX_PERSON(91002,"所选人数超过最大可选人数"),
    MAX_TIME(91003,"已超过总次数无法再投票了"),
    DAY_TIME(91004,"当天已投过票了无法再投了"),
    /**
     * 投票错误码 end
     */



    /**
     * 袋子机错误码 end
     */
    VEND_NULL(92001,"找不到袋子机"),
    VEND_BAG_SIZE(92002,"绑定袋子和剩余容量不匹配"),

    /**
     * 分类设备注册 start
     */
    DEVICE_BIN_LIST_NULL(93001,"分类设备注册时，垃圾类型不能为空"),
    /**
     * 分类设备注册 end
     */
    /**
     * 积分变动*/
    INTEGRAL_SHOR(94001,"积分不足"),
    /**
     * 绑定垃圾袋*/
    BAG_REPEAT(95001,"重复绑定垃圾袋"),
    BAG_MAX(95002,"本月免费领取次数已用完"),

    /**
     * 分类消息推送start*/
    NOTFOUND_RECEIVE_USER(95100,"未找到消息推送人"),
    /**
     * 分类消息推送end*/

    /**
     * 开户开卡start*/
    MOBILE_PHONE_REPEAT(95200,"用户手机号不能重复"),

    CARD_NO_REPEAT(95200,"实体卡号不能重复,请绑定正确的实体卡"),
    /**
     *人工回收
     */

    PERSONCOLLETC_PLACE_NULL(95300,"人工回收小区或垃圾类型为空"),
    PERSONCOLLETC_WEIGHI_NULL(95301,"重量为空"),
    NO_JF_RULE(95302,"您选择的垃圾类型还没有配置积分规则"),

    /**
     * 开户开卡end*/
    /**
     * 设备投放start*/
    ACCOUNT_NOT_REG(95400,"账号未注册"),
    DEVICE_NOT_REG(95401,"分类设备未注册"),
    GARBAGETYPE_NOT_CONFIG(95402,"积分规则未配置");
    /**
     * 设备投放end*/
    /**
     * 兑换机start*/

    /**
     * 兑换机end*/
    private int errCode;
    private String errMsg;

    public int getErrCode() {
        return errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    private ApiErrCode(int errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

}