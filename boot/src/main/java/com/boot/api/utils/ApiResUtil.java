package com.boot.api.utils;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ApiResUtil {
/**
* 错误码
 * 1：系统级
 * 2：服务级
 * */
    private static final String SUCCESS = "操作成功";

    private static final String FAILURE = "操作失败";

    private int errcode;

    private String errmsg;

    private Object data;

    public ApiResUtil(int errcode, String errmsg) {
        this(errcode, errmsg,null);
    }

    public ApiResUtil(int errcode, String errmsg, Object data) {
        this.errcode = errcode;
        this.errmsg = errmsg;
        this.data = data;
    }

    public static ApiResUtil succeed() {
        return new ApiResUtil(0,SUCCESS);
    }

    public static ApiResUtil succeed(Object obj) {
        return new ApiResUtil(0,SUCCESS,obj);
    }

    public static ApiResUtil succeed(String msg, Object obj) {
        return new ApiResUtil(0,msg,obj);
    }
    public static ApiResUtil succeed(int code,String msg) {
        return new ApiResUtil(code,msg);
    }
    public static ApiResUtil fail() {
        return new ApiResUtil(500,FAILURE);
    }
    public static ApiResUtil fail(Object obj) {
        return new ApiResUtil(-1,FAILURE,obj);
    }
    public static ApiResUtil fail(int code, String msg) {
        return new ApiResUtil(code,msg);
    }

    public int getErrcode() {
      return errcode;
    }

    public void setErrcode(int errcode) {
      this.errcode = errcode;
    }

    public String getErrmsg() {
      return errmsg;
    }

    public void setErrmsg(String errmsg) {
      this.errmsg = errmsg;
    }

    public Object getData() {
      return data;
    }

    public void setData(Object data) {
      this.data = data;
    }

}
