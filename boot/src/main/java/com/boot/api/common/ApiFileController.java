package com.boot.api.common;

import com.alibaba.fastjson.JSONObject;
import com.boot.api.utils.BASE64DecodedMultipartFile;
import com.boot.api.utils.Base64StrToImage;
import com.boot.common.config.BootdoConfig;
import com.boot.common.domain.FileDO;
import com.boot.common.service.FileService;
import com.boot.common.utils.FileType;
import com.boot.common.utils.FileUtil;
import com.boot.common.utils.IdGenerate;
import com.boot.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Enumeration;

@RestController
@RequestMapping("/api/common")
@CrossOrigin
public class ApiFileController {
    @Autowired
    private BootdoConfig bootdoConfig;
    @Autowired
    private FileService sysFileService;
    @ResponseBody
    @PostMapping("/uploadFile")
    R upload(@RequestBody JSONObject strs, HttpServletRequest request) throws Exception {
        System.out.println(strs);
       String file=strs.getString("file");
        BASE64DecodedMultipartFile base64DecodedMultipartFile = null;
        if(null != file && !file.isEmpty()){
            base64DecodedMultipartFile =  (BASE64DecodedMultipartFile) Base64StrToImage.base64MutipartFile(file);
        }

        String fileName = base64DecodedMultipartFile.getOriginalFilename();

        //   fileName = IdGenerate.uuid();
        System.out.println("开始上传文件啦>>>"+fileName);
        FileDO sysFile = new FileDO(FileType.fileType(fileName), "/files/" + fileName, new Date());
        FileUtil.uploadFile(base64DecodedMultipartFile.getBytes(), bootdoConfig.getUploadPath(), fileName);
//        try {
//            FileUtil.uploadFile(file.getBytes(), bootdoConfig.getUploadPath(), fileName);
//        } catch (Exception e) {
//            System.out.println(e);
//
//            return R.error();
//        }

        if (sysFileService.save(sysFile) > 0) {
            return R.ok().put("fileName",sysFile.getUrl());
        }
        return R.error();
    }
//    R upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
//
//        String fileName = file.getOriginalFilename();
//        String fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();
//        String name=fileName.substring(0, fileName.lastIndexOf(".") -1).toLowerCase();
//        fileName=name+IdGenerate.uuid()+"."+fileType;
//     //   fileName = IdGenerate.uuid();
//        System.out.println("开始上传文件啦>>>"+fileName);
//        FileDO sysFile = new FileDO(FileType.fileType(fileName), "/files/" + fileName, new Date());
//        FileUtil.uploadFile(file.getBytes(), bootdoConfig.getUploadPath(), fileName);
////        try {
////            FileUtil.uploadFile(file.getBytes(), bootdoConfig.getUploadPath(), fileName);
////        } catch (Exception e) {
////            System.out.println(e);
////
////            return R.error();
////        }
//
//        if (sysFileService.save(sysFile) > 0) {
//            return R.ok().put("fileName",sysFile.getUrl());
//        }
//        return R.error();
//    }
}
