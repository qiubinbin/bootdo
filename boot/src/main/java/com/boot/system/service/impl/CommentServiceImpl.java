package com.boot.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.boot.system.dao.CommentDao;
import com.boot.system.domain.CommentDO;
import com.boot.system.service.CommentService;



@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	private CommentDao commentDao;
	
	@Override
	public CommentDO get(String id){
		return commentDao.get(id);
	}
	
	@Override
	public List<CommentDO> list(Map<String, Object> map){
		return commentDao.list(map);
	}

	@Override
	public List<CommentDO> findListByMid(String mid) {
		return commentDao.findListByMid(mid);
	}

	@Override
	public int count(Map<String, Object> map){
		return commentDao.count(map);
	}
	
	@Override
	public int save(CommentDO comment){
		return commentDao.save(comment);
	}
	
	@Override
	public int update(CommentDO comment){
		return commentDao.update(comment);
	}
	
	@Override
	public int remove(String id){
		return commentDao.remove(id);
	}
	
	@Override
	public int batchRemove(String[] ids){
		return commentDao.batchRemove(ids);
	}
	
}
