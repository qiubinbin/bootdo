package com.boot.system.service;

import com.boot.system.domain.AnchorDO;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-08-06 20:04:07
 */
public interface AnchorService {
	
	AnchorDO get(String id);
	
	List<AnchorDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(AnchorDO anchor);
	
	int update(AnchorDO anchor);
	
	int remove(String id);
	
	int batchRemove(String[] ids);
}
