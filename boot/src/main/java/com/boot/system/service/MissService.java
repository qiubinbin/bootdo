package com.boot.system.service;

import com.boot.system.domain.MissDO;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-07-26 20:39:44
 */
public interface MissService {
	
	MissDO get(String id);
	
	List<MissDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(MissDO miss);
	
	int update(MissDO miss);
	
	int remove(String id);
	
	int batchRemove(String[] ids);
	List<MissDO> fingKey(String key);
}
