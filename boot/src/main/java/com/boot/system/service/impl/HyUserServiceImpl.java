package com.boot.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.boot.system.dao.HyUserDao;
import com.boot.system.domain.HyUserDO;
import com.boot.system.service.HyUserService;



@Service
public class HyUserServiceImpl implements HyUserService {
	@Autowired
	private HyUserDao hyUserDao;
	
	@Override
	public HyUserDO get(String id){
		return hyUserDao.get(id);
	}
	
	@Override
	public List<HyUserDO> list(Map<String, Object> map){
		return hyUserDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return hyUserDao.count(map);
	}
	
	@Override
	public int save(HyUserDO hyUser){
		return hyUserDao.save(hyUser);
	}
	
	@Override
	public int update(HyUserDO hyUser){
		return hyUserDao.update(hyUser);
	}
	
	@Override
	public int remove(String id){
		return hyUserDao.remove(id);
	}
	
	@Override
	public int batchRemove(String[] ids){
		return hyUserDao.batchRemove(ids);
	}
	
}
