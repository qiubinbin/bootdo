package com.boot.system.service;

import com.boot.system.domain.HyUserDO;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-08-06 20:03:22
 */
public interface HyUserService {
	
	HyUserDO get(String id);
	
	List<HyUserDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(HyUserDO hyUser);
	
	int update(HyUserDO hyUser);
	
	int remove(String id);
	
	int batchRemove(String[] ids);

}
