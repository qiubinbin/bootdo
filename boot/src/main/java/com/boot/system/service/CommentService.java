package com.boot.system.service;

import com.boot.system.domain.CommentDO;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-07-26 20:39:56
 */
public interface CommentService {
	
	CommentDO get(String id);
	
	List<CommentDO> list(Map<String, Object> map);
	List<CommentDO> findListByMid(String mid);

	int count(Map<String, Object> map);
	
	int save(CommentDO comment);
	
	int update(CommentDO comment);
	
	int remove(String id);
	
	int batchRemove(String[] ids);
}
