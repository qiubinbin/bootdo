package com.boot.system.service.impl;

import com.boot.common.utils.IdGenerate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.boot.system.dao.MissDao;
import com.boot.system.domain.MissDO;
import com.boot.system.service.MissService;



@Service
public class MissServiceImpl implements MissService {
	@Autowired
	private MissDao missDao;
	
	@Override
	public MissDO get(String id){
		return missDao.get(id);
	}
	
	@Override
	public List<MissDO> list(Map<String, Object> map){
		return missDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return missDao.count(map);
	}
	
	@Override
	public int save(MissDO miss){
		miss.setId(IdGenerate.uuid());
		return missDao.save(miss);
	}
	
	@Override
	public int update(MissDO miss){
		return missDao.update(miss);
	}
	
	@Override
	public int remove(String id){
		return missDao.remove(id);
	}
	
	@Override
	public int batchRemove(String[] ids){
		return missDao.batchRemove(ids);
	}

	@Override
	public List<MissDO> fingKey(String key) {
		return missDao.fingKey(key);
	}

}
