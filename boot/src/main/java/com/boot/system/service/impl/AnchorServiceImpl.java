package com.boot.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.boot.system.dao.AnchorDao;
import com.boot.system.domain.AnchorDO;
import com.boot.system.service.AnchorService;



@Service
public class AnchorServiceImpl implements AnchorService {
	@Autowired
	private AnchorDao anchorDao;
	
	@Override
	public AnchorDO get(String id){
		return anchorDao.get(id);
	}
	
	@Override
	public List<AnchorDO> list(Map<String, Object> map){
		return anchorDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return anchorDao.count(map);
	}
	
	@Override
	public int save(AnchorDO anchor){
		return anchorDao.save(anchor);
	}
	
	@Override
	public int update(AnchorDO anchor){
		return anchorDao.update(anchor);
	}
	
	@Override
	public int remove(String id){
		return anchorDao.remove(id);
	}
	
	@Override
	public int batchRemove(String[] ids){
		return anchorDao.batchRemove(ids);
	}
	
}
