package com.boot.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-08-06 20:04:07
 */
public class AnchorDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//主播名字
	private String anchorName;
	//主播头像
	private String anchorImg;
	//房间号
	private String anchorNo;
	//参与人数
	private Integer userNum;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	//创建时间
	private Date createDate;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：主播名字
	 */
	public void setAnchorName(String anchorName) {
		this.anchorName = anchorName;
	}
	/**
	 * 获取：主播名字
	 */
	public String getAnchorName() {
		return anchorName;
	}
	/**
	 * 设置：主播头像
	 */
	public void setAnchorImg(String anchorImg) {
		this.anchorImg = anchorImg;
	}
	/**
	 * 获取：主播头像
	 */
	public String getAnchorImg() {
		return anchorImg;
	}
	/**
	 * 设置：房间号
	 */
	public void setAnchorNo(String anchorNo) {
		this.anchorNo = anchorNo;
	}
	/**
	 * 获取：房间号
	 */
	public String getAnchorNo() {
		return anchorNo;
	}
	/**
	 * 设置：参与人数
	 */
	public void setUserNum(Integer userNum) {
		this.userNum = userNum;
	}
	/**
	 * 获取：参与人数
	 */
	public Integer getUserNum() {
		return userNum;
	}

}
