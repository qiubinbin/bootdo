package com.boot.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-07-26 20:39:56
 */
public class CommentDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//missid
	private String missId;
	//回复内容
	private String cont;
	//创建时间
	private Date createDate;
	//用户名
	private String userName;
	//头像
	private String userPic;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：missid
	 */
	public void setMissId(String missId) {
		this.missId = missId;
	}
	/**
	 * 获取：missid
	 */
	public String getMissId() {
		return missId;
	}
	/**
	 * 设置：回复内容
	 */
	public void setCont(String cont) {
		this.cont = cont;
	}
	/**
	 * 获取：回复内容
	 */
	public String getCont() {
		return cont;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * 设置：用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * 获取：用户名
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * 设置：头像
	 */
	public void setUserPic(String userPic) {
		this.userPic = userPic;
	}
	/**
	 * 获取：头像
	 */
	public String getUserPic() {
		return userPic;
	}
}
