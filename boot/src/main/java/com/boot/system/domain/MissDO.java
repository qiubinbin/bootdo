package com.boot.system.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-07-26 20:39:44
 */
public class MissDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id;
	//失踪人姓名
	private String missName;
	//失踪人性别
	private String missSex;
	//失踪人身高
	private Double missHeight;
	//失踪人年龄
	private Integer missAge;
	//失踪类型
	private String missType;
	//籍贯
	private String missPlace;
	//详细地点
	private String missAddress;
	//可疑去向
	private String missSense;
	//其他特征
	private String missFeatures;
	//寻人启事
	private String missInfo;
	//列表展示
	private String missImg;
	//相关照片
	private String missImgs;
	//请求人姓名
	private String requestName;
	//请求人电话
	private String requestPhone;
	//补充资料
	private String requestOther;
	//线索酬金
	private String clueMoney;
	//送回酬金
	private String returnMoney;
	//创建时间
	private Date createDate;

	public List<CommentDO> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<CommentDO> commentList) {
		this.commentList = commentList;
	}

	private List<CommentDO> commentList;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：失踪人姓名
	 */
	public void setMissName(String missName) {
		this.missName = missName;
	}
	/**
	 * 获取：失踪人姓名
	 */
	public String getMissName() {
		return missName;
	}
	/**
	 * 设置：失踪人性别
	 */
	public void setMissSex(String missSex) {
		this.missSex = missSex;
	}
	/**
	 * 获取：失踪人性别
	 */
	public String getMissSex() {
		return missSex;
	}
	/**
	 * 设置：失踪人身高
	 */
	public void setMissHeight(Double missHeight) {
		this.missHeight = missHeight;
	}
	/**
	 * 获取：失踪人身高
	 */
	public Double getMissHeight() {
		return missHeight;
	}
	/**
	 * 设置：失踪人年龄
	 */
	public void setMissAge(Integer missAge) {
		this.missAge = missAge;
	}
	/**
	 * 获取：失踪人年龄
	 */
	public Integer getMissAge() {
		return missAge;
	}
	/**
	 * 设置：失踪类型
	 */
	public void setMissType(String missType) {
		this.missType = missType;
	}
	/**
	 * 获取：失踪类型
	 */
	public String getMissType() {
		return missType;
	}
	/**
	 * 设置：籍贯
	 */
	public void setMissPlace(String missPlace) {
		this.missPlace = missPlace;
	}
	/**
	 * 获取：籍贯
	 */
	public String getMissPlace() {
		return missPlace;
	}
	/**
	 * 设置：详细地点
	 */
	public void setMissAddress(String missAddress) {
		this.missAddress = missAddress;
	}
	/**
	 * 获取：详细地点
	 */
	public String getMissAddress() {
		return missAddress;
	}
	/**
	 * 设置：可疑去向
	 */
	public void setMissSense(String missSense) {
		this.missSense = missSense;
	}
	/**
	 * 获取：可疑去向
	 */
	public String getMissSense() {
		return missSense;
	}
	/**
	 * 设置：其他特征
	 */
	public void setMissFeatures(String missFeatures) {
		this.missFeatures = missFeatures;
	}
	/**
	 * 获取：其他特征
	 */
	public String getMissFeatures() {
		return missFeatures;
	}
	/**
	 * 设置：寻人启事
	 */
	public void setMissInfo(String missInfo) {
		this.missInfo = missInfo;
	}
	/**
	 * 获取：寻人启事
	 */
	public String getMissInfo() {
		return missInfo;
	}
	/**
	 * 设置：列表展示
	 */
	public void setMissImg(String missImg) {
		this.missImg = missImg;
	}
	/**
	 * 获取：列表展示
	 */
	public String getMissImg() {
		return missImg;
	}
	/**
	 * 设置：相关照片
	 */
	public void setMissImgs(String missImgs) {
		this.missImgs = missImgs;
	}
	/**
	 * 获取：相关照片
	 */
	public String getMissImgs() {
		return missImgs;
	}
	/**
	 * 设置：请求人姓名
	 */
	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}
	/**
	 * 获取：请求人姓名
	 */
	public String getRequestName() {
		return requestName;
	}
	/**
	 * 设置：请求人电话
	 */
	public void setRequestPhone(String requestPhone) {
		this.requestPhone = requestPhone;
	}
	/**
	 * 获取：请求人电话
	 */
	public String getRequestPhone() {
		return requestPhone;
	}
	/**
	 * 设置：补充资料
	 */
	public void setRequestOther(String requestOther) {
		this.requestOther = requestOther;
	}
	/**
	 * 获取：补充资料
	 */
	public String getRequestOther() {
		return requestOther;
	}
	/**
	 * 设置：线索酬金
	 */
	public void setClueMoney(String clueMoney) {
		this.clueMoney = clueMoney;
	}
	/**
	 * 获取：线索酬金
	 */
	public String getClueMoney() {
		return clueMoney;
	}
	/**
	 * 设置：送回酬金
	 */
	public void setReturnMoney(String returnMoney) {
		this.returnMoney = returnMoney;
	}
	/**
	 * 获取：送回酬金
	 */
	public String getReturnMoney() {
		return returnMoney;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}
}
