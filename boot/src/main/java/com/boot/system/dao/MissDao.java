package com.boot.system.dao;

import com.boot.system.domain.MissDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-07-26 20:39:44
 */
@Mapper
public interface MissDao {

	MissDO get(String id);
	List<MissDO> fingKey(@Param("key") String key);
	List<MissDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(MissDO miss);
	
	int update(MissDO miss);
	
	int remove(String id);
	
	int batchRemove(String[] ids);
}
