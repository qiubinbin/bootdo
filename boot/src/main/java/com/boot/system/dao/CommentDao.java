package com.boot.system.dao;

import com.boot.system.domain.CommentDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-07-26 20:39:56
 */
@Mapper
public interface CommentDao {

	CommentDO get(String id);
	
	List<CommentDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(CommentDO comment);
	
	int update(CommentDO comment);
	
	int remove(String id);
	
	int batchRemove(String[] ids);
	List<CommentDO> findListByMid(@Param("mid") String mid);
}
