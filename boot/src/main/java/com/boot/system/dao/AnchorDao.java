package com.boot.system.dao;

import com.boot.system.domain.AnchorDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-08-06 20:04:07
 */
@Mapper
public interface AnchorDao {

	AnchorDO get(String id);
	
	List<AnchorDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(AnchorDO anchor);
	
	int update(AnchorDO anchor);
	
	int remove(String id);
	
	int batchRemove(String[] ids);
}
