package com.boot.system.dao;

import com.boot.system.domain.HyUserDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-08-06 20:03:22
 */
@Mapper
public interface HyUserDao {

	HyUserDO get(String id);
	
	List<HyUserDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(HyUserDO hyUser);
	
	int update(HyUserDO hyUser);
	
	int remove(String id);
	
	int batchRemove(String[] ids);
}
