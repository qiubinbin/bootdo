package com.boot.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.boot.system.domain.AnchorDO;
import com.boot.system.service.AnchorService;
import com.boot.common.utils.PageUtils;
import com.boot.common.utils.Query;
import com.boot.common.utils.R;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-08-06 20:04:07
 */
 
@Controller
@RequestMapping("/system/anchor")
public class AnchorController {
	@Autowired
	private AnchorService anchorService;
	
	@GetMapping()
	@RequiresPermissions("system:anchor:anchor")
	String Anchor(){
	    return "system/anchor/anchor";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:anchor:anchor")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<AnchorDO> anchorList = anchorService.list(query);
		int total = anchorService.count(query);
		PageUtils pageUtils = new PageUtils(anchorList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:anchor:add")
	String add(){
	    return "system/anchor/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:anchor:edit")
	String edit(@PathVariable("id") String id,Model model){
		AnchorDO anchor = anchorService.get(id);
		model.addAttribute("anchor", anchor);
	    return "system/anchor/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:anchor:add")
	public R save( AnchorDO anchor){
		if(anchorService.save(anchor)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:anchor:edit")
	public R update( AnchorDO anchor){
		anchorService.update(anchor);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:anchor:remove")
	public R remove( String id){
		if(anchorService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:anchor:batchRemove")
	public R remove(@RequestParam("ids[]") String[] ids){
		anchorService.batchRemove(ids);
		return R.ok();
	}
	
}
