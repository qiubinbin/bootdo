package com.boot.system.controller;

import java.util.List;
import java.util.Map;

import com.boot.common.utils.IdGenerate;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.boot.system.domain.MissDO;
import com.boot.system.service.MissService;
import com.boot.common.utils.PageUtils;
import com.boot.common.utils.Query;
import com.boot.common.utils.R;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-07-26 20:39:44
 */
 
@Controller
@RequestMapping("/system/miss")
public class MissController {
	@Autowired
	private MissService missService;
	
	@GetMapping()
	@RequiresPermissions("system:miss:miss")
	String Miss(){
	    return "system/miss/miss";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:miss:miss")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<MissDO> missList = missService.list(query);
		int total = missService.count(query);
		PageUtils pageUtils = new PageUtils(missList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:miss:add")
	String add(){
	    return "system/miss/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:miss:edit")
	String edit(@PathVariable("id") String id,Model model){
		MissDO miss = missService.get(id);
		model.addAttribute("miss", miss);
	    return "system/miss/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:miss:add")
	public R save( MissDO miss){
		miss.setId(IdGenerate.uuid());
		if(missService.save(miss)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:miss:edit")
	public R update( MissDO miss){
		missService.update(miss);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:miss:remove")
	public R remove( String id){
		if(missService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:miss:batchRemove")
	public R remove(@RequestParam("ids[]") String[] ids){
		missService.batchRemove(ids);
		return R.ok();
	}
	
}
