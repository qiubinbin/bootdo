package com.boot.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.boot.system.domain.HyUserDO;
import com.boot.system.service.HyUserService;
import com.boot.common.utils.PageUtils;
import com.boot.common.utils.Query;
import com.boot.common.utils.R;

/**
 * 
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-08-06 20:03:22
 */
 
@Controller
@RequestMapping("/system/hyUser")
public class HyUserController {
	@Autowired
	private HyUserService hyUserService;
	
	@GetMapping()
	@RequiresPermissions("system:hyUser:hyUser")
	String HyUser(){
	    return "system/hyUser/hyUser";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:hyUser:hyUser")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<HyUserDO> hyUserList = hyUserService.list(query);
		int total = hyUserService.count(query);
		PageUtils pageUtils = new PageUtils(hyUserList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:hyUser:add")
	String add(){
	    return "system/hyUser/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:hyUser:edit")
	String edit(@PathVariable("id") String id,Model model){
		HyUserDO hyUser = hyUserService.get(id);
		model.addAttribute("hyUser", hyUser);
	    return "system/hyUser/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:hyUser:add")
	public R save( HyUserDO hyUser){
		if(hyUserService.save(hyUser)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:hyUser:edit")
	public R update( HyUserDO hyUser){
		hyUserService.update(hyUser);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:hyUser:remove")
	public R remove( String id){
		if(hyUserService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:hyUser:batchRemove")
	public R remove(@RequestParam("ids[]") String[] ids){
		hyUserService.batchRemove(ids);
		return R.ok();
	}
	
}
